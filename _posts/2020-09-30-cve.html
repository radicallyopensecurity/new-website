---
layout: post
title:  "GAEN Protocol Metadata Deanonymization and Risk-score Inflation Issues"
summary: "The TX Power value in the metadata in the beacon of the GAEN protocol used by the corona/contact tracing app allows for attackers to influence risk-score calculations in their favor, the same metadata can also be used to deanonymize diagnosed users based on the type of phone they are using."
author: Team
banner-divider: false
featured-image:
background-color: light
---

<section class="post">
    <h2 id="summary">Summary</h2>
    <p>The TX Power value in the metadata in the beacon of the GAEN protocol used by the corona/contact tracing app allows for attackers to influence risk-score calculations in their favor, the same metadata can also be used to deanonymize diagnosed users based on the type of phone they are using.</p>
    <h2 id="intro-gaen-metadata-in-a-nutshell">Intro: GAEN Metadata in a nutshell</h2>
    <p>The beacon sent out by the protocol contains some metadata. This metadata is encrypted with the Associated Encrypted Metadata Key (AEMK), which is derived from the current TEK as follows:</p>
    <pre><code>AEMK = HDKF(TEK, NULL, "EN-AEMK", 16)</code></pre>
    <p>The metadata is only 4 bytes in length and is currently specified as follows:</p>
    <pre><code>Byte 0 — Versioning. - Bits 7:6 — Major version (01).
                     - Bits 5:4 — Minor version (00).
                     - Bits 3:0 — Reserved for future use.
Byte 1 — Transmit power level. - This is the measured radiated
      transmit power of Bluetooth Advertisement packets,
      and is used to improve distance approximation. The
      range of this field shall be -127 to +127 dBm.
Byte 2-3 — Reserved for future use.</code></pre>
    <p>The four bytes of metadata are encrypted like this:</p>
    <pre><code>AEM=aes128ctr(AEMK,RPI,Metadata)</code></pre>
    <p>Here, AEMK is used as the key, RPI is used as the initialization vector (IV), and Metadata as the plaintext input to an AES128 in a Counter Mode-like construction. Counter Mode acts as a stream cipher in this construction and only uses the first 4 bytes of its output, and thus no padding is needed. However AES in Counter Mode does not provide authenticity of the encrypted content. This means that a re(p)laying attacker can flip bits in a controlled way[1].</p>
    <p>[1] <a href="https://lasec.epfl.ch/people/vaudenay/swisscovid/swisscovid-ana.pdf">https://lasec.epfl.ch/people/vaudenay/swisscovid/swisscovid-ana.pdf</a> p 12, section 3.5</p>
    <h2 id="issue-1-increasing-risk-scores-by-bit-flipping-tx-power-in-the-metadata">Issue 1: Increasing risk scores by bit-flipping TX Power in the Metadata</h2>
    <p>The problem is that the TX power value is not uniform furthermore it can be known for transmitting devices and can thus be manipulated through bit-flipping to make re(p)layed beacons appear closer than in reality and thus increasing the risk-score of the victim.</p>
    <p>There are multiple variants of this attack based on the knowledge of the attacker about the re(p)layed beacon sending device:</p>
    <ol type="1">
        <li>if the attacker knows the exact type of the sending device the TX power value is known, and can thus be safely manipulated to the lowest known valid dB value which is -46dB. With the bulk of android devices having a TX Power value of more than -30dB this is a significant change.</li>
        <li>If the attacker knows some probability distribution of possible sending devices it is possible to calculate the odds of bitflips which have a high chance of flipping the TX Power value to a lower but still valid value.</li>
        <li>If the attacker has no knowledge of the sending device it is still possible to calculate the most hopeful combination of bitflips leading to an increase in risk-scoring. An example of such a calculation is appended in C source code to this report. In this case there is still a ~65% chance (by flipping bits 2 and 3) to make the received beacons appear closer than they really are - while the manipulated TX power value will flip to a value that is valid for some other device.</li>
        <li>If the attacker does not care about TX Power values that do not correspond to existing devices, it is also possible to flip bit 6 of the TX power and thus generate a value that is not associated with any device, but which will with very high probability register as a very close contact.</li>
    </ol>
    <p>An attacker may also send multiple attempts flipping various bits; the validation during checking might weed out the invalid ones, and keep the valid but stronger values. This is speculation and depends on the implementation handling multiple Beacons with the same RPI but different AEM values and the validation of these in case they are decrypted with the Diagnosis key.</p>
    <p>Impact: A re(p)laying attacker can can improve his attack by influencing the risk-score calculation.</p>
    <h2 id="issue-2-deanonymizing-diagnosed-users-based-on-devices-with-unique-or-uncommon-tx-power-values">Issue 2: Deanonymizing Diagnosed users based on devices with unique or uncommon TX Power values</h2>
    <p>The value of the Transmit power level is a device-dependant value[2]; values for Android phones are published by Google[3]. Some of these values are unique to a specific device, as in the case of the Pixel 3a, SM-A510M, SM-G610F, and SM-J510F. This means that diagnosed victims with these devices are easier to deanonymize, if they are known to be in possession of such a device. This attack can be extended to a probabilistic one when combined with statistical information on which devices are in common use in a certain geographic location (for example based on regional sales statistics or local fashion trends).</p>
    <p>The GAEN system sets the TX power based on the device and includes this information in the AEM metadata[4][5]. By combining beacons of infected persons in conjunction with an infected person’s diagnosis key it is possible to infer which device these people were using.</p>
    <figure>
        <img src="https://blog.radicallyopensecurity.com/txpowerdistribution.png" alt="TX power distribution of Android devices&quot;" width="100%"><figcaption>TX power distribution of Android devices"</figcaption>
    </figure>
    <p>In the bar graph above, the X-axis shows the value of TX power sent, and in the Y-axis shows how many different devices set this value. Devices with low values on the Y-axis are TX values that should be easy to identify. Notably, the Pixel 3a, SM-A510M, SM-G610F and SM-J510F all have unique values. If these values are seen in the Metadata, the sending device can be unambiguously identified.</p>
    <p>Impact: Infected persons can be easier (sometimes trivially) deanonymized by entities having collected their beacons based on the TX Power value in the Metadata.</p>
    <p>[2] <a href="https://developers.google.com/android/exposure-notifications/ble-attenuation-computation">https://developers.google.com/android/exposure-notifications/ble-attenuation-computation</a> [3] <a href="https://developers.google.com/android/exposure-notifications/files/en-calibration-2020-06-13.csv">https://developers.google.com/android/exposure-notifications/files/en-calibration-2020-06-13.csv</a> [4] <a href="https://developers.google.com/android/exposure-notifications/ble-attenuation-overview">https://developers.google.com/android/exposure-notifications/ble-attenuation-overview</a> [5] <a href="https://blog.google/documents/70/Exposure_Notification_-_Bluetooth_Specification_v1.2.2.pdf">https://blog.google/documents/70/Exposure_Notification_-_Bluetooth_Specification_v1.2.2.pdf</a></p>
    <h2 id="timeline">Timeline</h2>
    <ul>
        <li>2020 Jun  5 - Vaudenay, Vuagnoux report mentions malleability of Metadata</li>
        <li>2020 Aug 27 - Request by client to disclose these issues to Google, Apple and the dutch National Cyber-Security Center (NCSC)</li>
        <li>2020 Aug 28 - Full Report shared with Dutch Ministry of Health</li>
        <li>2020 Aug 28 - Submitted to the Google Android Security Bugtracker</li>
        <li>2020 Aug 28 - Apple acknowledges receipt of report</li>
        <li>2020 Aug 31 - NCSC confirmation of receipt</li>
        <li>2020 Sep  1 - Google assigns issue to the Android engineering team.</li>
        <li>2020 Sep  7 - Google responds to issue I- see below</li>
        <li>2020 Sep 29 - Full Report gets published</li>
        <li>2020 Oct 05 - Detailed write-up of CVE-2020-24722 gets published</li>
    </ul>
    <h3 id="google-response-2020-sep-7">Google Response (2020 Sep 7)</h3>
    <blockquote>
        <p>Thanks again for your report. We understand and have analyzed the concern that Associated Encrypted Metadata (AEM) TX (transmit) power could be altered as part of a relay attack. We do not believe that TX power authentication would be a useful defense against relay attacks for the following reasons:</p>
        <ul>
            <li><p>TX power authentication doesn’t protect against the use of a high-power antenna positioned to cover a large area with a signal strength indicative of proximity. This is because the system is designed to be highly tolerant of differences between TX-power and received signal strength indicator (RSSI) caused, for example, by both devices being in-pocket. The Android ecosystem also has a diverse range of hardware: some Android devices transmit signals as much as 30dB weaker than others. The required system tolerance to differences in transmit power means that relaying a packet from such a weak device on a higher transmit power device would already allow the higher power device to appear nearby without altering the TX field.</p></li>
            <li><p>TX power authentication would not prevent the deployment of malware on phones to perform relay packets; app-store and OS policy are more effective to prevent collection of EN packets by third party apps.</p></li>
            <li><p>In this context, encrypting the AEM prevents an adversary from joining between RPIs coming from the same device (using device-specific TX power as a source of entropy).</p></li>
        </ul>
        <p>Please see <a href="https://github.com/google/exposure-notifications-internals/blob/main/en-risks-and-mitigations-faq.md#additional-considerations">https://github.com/google/exposure-notifications-internals/blob/main/en-risks-and-mitigations-faq.md#additional-considerations</a> for additional information.</p>
    </blockquote>
    <h1 id="appendix-i---example-calculation-of-best-bitflips">Appendix I - example calculation of best bitflips</h1>
    <div class="sourceCode" id="cb5" style="flex-wrap: wrap;">
    <pre class="sourceCode c"><code class="sourceCode c">

        <a class="sourceLine" id="cb5-1" title="1"><span class="pp">#include </span><span class="im">&lt;stdio.h&gt;</span></a>
        <a class="sourceLine" id="cb5-2" title="2"></a>
        <a class="sourceLine" id="cb5-3" title="3"><span class="kw">typedef</span> <span class="kw">struct</span> {</a>
        <a class="sourceLine" id="cb5-4" title="4">  <span class="dt">char</span> tx;</a>
        <a class="sourceLine" id="cb5-5" title="5">  <span class="dt">int</span> cnt;</a>
        <a class="sourceLine" id="cb5-6" title="6">} TX_Count;</a>
        <a class="sourceLine" id="cb5-7" title="7"></a>
        <a class="sourceLine" id="cb5-8" title="8"><span class="co">// source: </span></a><a href="https://developers.google.com/android/exposure-notifications/files/en-calibration-2020-06-13.csv">https://developers.google.com/android/exposure-notifications/files/en-calibration-2020-06-13.csv</a>
        <a class="sourceLine" id="cb5-9" title="9"><span class="co">// cnt is count of all items with the given tx value</span></a>
        <a class="sourceLine" id="cb5-10" title="10">TX_Count calib[] = \{\{-<span class="dv">46</span>, <span class="dv">3</span>}, {-<span class="dv">45</span>, <span class="dv">2</span>}, {-<span class="dv">41</span>, <span class="dv">32</span>}, {-<span class="dv">40</span>, <span class="dv">5</span>}, {-<span class="dv">39</span>, <span class="dv">1</span>}, {-<span class="dv">38</span>, <span class="dv">4</span>}, {-<span class="dv">37</span>, <span class="dv">11</span>},</a>
        <a class="sourceLine" id="cb5-11" title="11">                  {-<span class="dv">36</span>, <span class="dv">15</span>}, {-<span class="dv">35</span>, <span class="dv">75</span>}, {-<span class="dv">34</span>, <span class="dv">11</span>}, {-<span class="dv">33</span>, <span class="dv">223</span>}, {-<span class="dv">32</span>, <span class="dv">22</span>}, {-<span class="dv">31</span>, <span class="dv">83</span>},</a>
        <a class="sourceLine" id="cb5-12" title="12">                  {-<span class="dv">30</span>, <span class="dv">851</span>}, {-<span class="dv">29</span>, <span class="dv">76</span>}, {-<span class="dv">28</span>, <span class="dv">262</span>}, {-<span class="dv">27</span>, <span class="dv">522</span>}, {-<span class="dv">26</span>, <span class="dv">1025</span>}, {-<span class="dv">25</span>, <span class="dv">1414</span>},</a>
        <a class="sourceLine" id="cb5-13" title="13">                  {-<span class="dv">24</span>, <span class="dv">2075</span>}, {-<span class="dv">23</span>, <span class="dv">35</span>}, {-<span class="dv">22</span>, <span class="dv">245</span>}, {-<span class="dv">21</span>, <span class="dv">67</span>}, {-<span class="dv">20</span>, <span class="dv">1037</span>}, {-<span class="dv">19</span>, <span class="dv">1371</span>},</a>
        <a class="sourceLine" id="cb5-14" title="14">                  {-<span class="dv">18</span>, <span class="dv">255</span>}, {-<span class="dv">17</span>, <span class="dv">14</span>}, {-<span class="dv">16</span>, <span class="dv">32</span>}, {-<span class="dv">15</span>, <span class="dv">10</span>}, {-<span class="dv">14</span>, <span class="dv">2</span>}, {-<span class="dv">13</span>, <span class="dv">1</span>}, {-<span class="dv">12</span>, <span class="dv">16</span>},</a>
        <a class="sourceLine" id="cb5-15" title="15">                  {-<span class="dv">9</span>, <span class="dv">16</span>}, {-<span class="dv">7</span>, <span class="dv">6</span>}, {-<span class="dv">6</span>, <span class="dv">27</span>}, {-<span class="dv">5</span>, <span class="dv">4</span>}, {-<span class="dv">3</span>, <span class="dv">12</span>}, {-<span class="dv">2</span>, <span class="dv">5</span>}, {-<span class="dv">1</span>, <span class="dv">1</span>}, {<span class="dv">0</span>, <span class="dv">27</span>}, {<span class="dv">2</span>, <span class="dv">1</span>},</a>
        <a class="sourceLine" id="cb5-16" title="16">                  {<span class="dv">0</span>,<span class="dv">0</span>}};</a>
        <a class="sourceLine" id="cb5-17" title="17"></a>
        <a class="sourceLine" id="cb5-18" title="18"><span class="dt">int</span> main(<span class="dt">void</span>) {</a>
        <a class="sourceLine" id="cb5-19" title="19">  <span class="dt">int</span> bits[<span class="dv">7</span>] = {<span class="dv">0</span>};</a>
        <a class="sourceLine" id="cb5-20" title="20">  <span class="dt">int</span> i, j, k, total = <span class="dv">0</span>;</a>
        <a class="sourceLine" id="cb5-21" title="21">  <span class="co">// calculate total of all cnt values</span></a>
        <a class="sourceLine" id="cb5-22" title="22">  <span class="cf">for</span>(i=<span class="dv">0</span>;calib[i].cnt!=<span class="dv">0</span>;i++)</a>
        <a class="sourceLine" id="cb5-23" title="23">    total+=calib[i].cnt;</a>
        <a class="sourceLine" id="cb5-24" title="24">  <span class="co">// test each tx_count in calib</span></a>
        <a class="sourceLine" id="cb5-25" title="25">  <span class="cf">for</span>(i=<span class="dv">0</span>;calib[i].cnt!=<span class="dv">0</span>;i++) {</a>
        <a class="sourceLine" id="cb5-26" title="26">    <span class="cf">for</span>(j=<span class="dv">0</span>;j&lt;(<span class="dt">int</span>)<span class="kw">sizeof</span> bits;j++) {</a>
        <a class="sourceLine" id="cb5-27" title="27">      <span class="dt">char</span> flipped = calib[i].tx ^ (<span class="dv">1</span> &lt;&lt; j);</a>
        <a class="sourceLine" id="cb5-28" title="28"><span class="pp">#ifdef noisy</span></a>
        <a class="sourceLine" id="cb5-29" title="29">      <span class="cf">if</span>(flipped &lt; calib[i].tx) bits[j]+=calib[i].cnt;</a>
        <a class="sourceLine" id="cb5-30" title="30"><span class="pp">#else</span></a>
        <a class="sourceLine" id="cb5-31" title="31">      <span class="cf">for</span>(k=<span class="dv">0</span>;calib[k].cnt!=<span class="dv">0</span>;k++) {</a>
        <a class="sourceLine" id="cb5-32" title="32">        <span class="cf">if</span>(calib[k].tx == flipped) {</a>
        <a class="sourceLine" id="cb5-33" title="33">          <span class="cf">if</span>(calib[k].tx &lt; calib[i].tx) bits[j]+=calib[i].cnt;</a>
        <a class="sourceLine" id="cb5-34" title="34">          <span class="cf">break</span>;</a>
        <a class="sourceLine" id="cb5-35" title="35">        }</a>
        <a class="sourceLine" id="cb5-36" title="36">      }</a>
        <a class="sourceLine" id="cb5-37" title="37"><span class="pp">#endif</span></a>
        <a class="sourceLine" id="cb5-38" title="38">    }</a>
        <a class="sourceLine" id="cb5-39" title="39">  }</a>
        <a class="sourceLine" id="cb5-40" title="40">  <span class="cf">for</span>(i=<span class="dv">0</span>;i&lt;<span class="dv">7</span>;i++) {</a>
        <a class="sourceLine" id="cb5-41" title="41">    printf(<span class="st">"bit %d: %d (%2.3f%%)</span><span class="sc">\n</span><span class="st">"</span>, i, bits[i], (bits[i]/(<span class="dt">float</span>)total)*<span class="dv">100</span>);</a>
        <a class="sourceLine" id="cb5-42" title="42">  }</a>
        <a class="sourceLine" id="cb5-43" title="43">  <span class="cf">return</span> <span class="dv">0</span>;</a>
        <a class="sourceLine" id="cb5-44" title="44">}</a>
        <a class="sourceLine" id="cb5-45" title="45"></a>
        <a class="sourceLine" id="cb5-46" title="46"><span class="co">// outputs</span></a>
        <a class="sourceLine" id="cb5-47" title="47"><span class="co">// bit 0: 3910 (39.511%)</span></a>
        <a class="sourceLine" id="cb5-48" title="48"><span class="co">// bit 1: 4205 (42.492%)</span></a>
        <a class="sourceLine" id="cb5-49" title="49"><span class="co">// bit 2: 6306 (63.723%)</span></a>
        <a class="sourceLine" id="cb5-50" title="50"><span class="co">// bit 3: 5375 (54.315%)</span></a>
        <a class="sourceLine" id="cb5-51" title="51"><span class="co">// bit 4: 132 (1.334%)</span></a>
        <a class="sourceLine" id="cb5-52" title="52"><span class="co">// bit 5: 74 (0.748%)</span></a>
        <a class="sourceLine" id="cb5-53" title="53"><span class="co">// bit 6: 0 (0.000%)</span></a>
        <a class="sourceLine" id="cb5-54" title="54"><span class="co">// flipping bits 2+3 from 00 to 11 has a chance of: 34.61114745% increasing risk by 12db</span></a>
        <a class="sourceLine" id="cb5-55" title="55"><span class="co">//               2+3 from 10 to 01 has a chance of 29.111852549999995% increasing risk by 4db</span></a>
        <a class="sourceLine" id="cb5-56" title="56"><span class="co">//               2+3 from 01 to 10 has a chance of 19.703852550000004% decreasing risk by 4db</span></a>
        <a class="sourceLine" id="cb5-57" title="57"><span class="co">//               2+3 from 11 to 00 has a chance of 16.57314745% decreasing risk by 12db</span></a>
        <a class="sourceLine" id="cb5-58" title="58"></a>
        <a class="sourceLine" id="cb5-59" title="59"></a>
        <a class="sourceLine" id="cb5-60" title="60"><span class="co">// flipping bits 2+3 from 00 to 11: 34.61114745% -12db</span></a>
        <a class="sourceLine" id="cb5-61" title="61"><span class="co">//               10 to 01: 29.111852549999995% -4db</span></a>
        <a class="sourceLine" id="cb5-62" title="62"><span class="co">//               01 to 10: 19.703852550000004% +4db</span></a>
        <a class="sourceLine" id="cb5-63" title="63"><span class="co">//               11 to 00: 16.57314745% +12db</span></a>
        <a class="sourceLine" id="cb5-64" title="64"></a>
        <a class="sourceLine" id="cb5-65" title="65"><span class="co">// noisy variant</span></a>
        <a class="sourceLine" id="cb5-66" title="66"><span class="co">// bit 0: 3976 (40.178%)</span></a>
        <a class="sourceLine" id="cb5-67" title="67"><span class="co">// bit 1: 4290 (43.351%)</span></a>
        <a class="sourceLine" id="cb5-68" title="68"><span class="co">// bit 2: 6306 (63.723%)</span></a>
        <a class="sourceLine" id="cb5-69" title="69"><span class="co">// bit 3: 5499 (55.568%)</span></a>
        <a class="sourceLine" id="cb5-70" title="70"><span class="co">// bit 4: 514 (5.194%)</span></a>
        <a class="sourceLine" id="cb5-71" title="71"><span class="co">// bit 5: 9486 (95.857%)</span></a>
        <a class="sourceLine" id="cb5-72" title="72"><span class="co">// bit 6: 9868 (99.717%)</span></a>
    </code></pre>
    </div>
</section>