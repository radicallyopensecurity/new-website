## Adding a service to the our services page

1. The services are managed by the yml file in the directory `_data/our-services.yml`
1. To add a service edit the file and add a new value like shown below. `text` doesn't need to be filled here.

**Important:** In Yaml it is important to keep the indenting the same otherwise it will not work.
```
- title: 'Penetration Testing + Ethical Hacks + Social Engineering'
  text:
```